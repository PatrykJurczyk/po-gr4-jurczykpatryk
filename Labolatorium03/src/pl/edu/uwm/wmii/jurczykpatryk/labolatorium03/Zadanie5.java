package pl.edu.uwm.wmii.jurczykpatryk.labolatorium03;

import java.math.BigDecimal;
import java.util.Scanner;

public class Zadanie5 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double k=sc.nextInt();
        double p=sc.nextInt();
        int n=sc.nextInt();
        System.out.println(wielkoscKapital(k,p,n));
    }

    public static String wielkoscKapital(double k,double p, int n) {
        double pom=1+(p/100);
        BigDecimal rezultat=new BigDecimal(Double.toString(pom));
        BigDecimal pom1 =new BigDecimal(Double.toString(k));
        rezultat=rezultat.pow(n);
        rezultat=rezultat.multiply(pom1);
        return String.format("%.2f",rezultat);
    }
}