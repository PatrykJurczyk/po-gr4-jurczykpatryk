package pl.edu.uwm.wmii.jurczykpatryk.labolatorium03;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Zadanie2 {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj znak jeden znak!!!!: ");
        String pom;
        char znak;
        pom = sc.nextLine();
        znak = pom.charAt(0);
        System.out.print("Podaj sciezke do pliku: ");
        String sciezka=sc.nextLine();
        System.out.println("Tyle jest "+"'"+znak+"'"+" : "+ileJestZnakow(sciezka,znak));
    }

    public static int ileJestZnakow(String name , char c) throws FileNotFoundException {
        Scanner file = new Scanner(new File(name));
        String plikclone="";
        int wynik=0;
        while (file.hasNextLine()) {
            plikclone += file.nextLine();
        }
        for(int i=0;i<plikclone.length();i++){
            if(plikclone.charAt(i)==c){
                wynik++;
            }
        }
        return wynik;
    }
}