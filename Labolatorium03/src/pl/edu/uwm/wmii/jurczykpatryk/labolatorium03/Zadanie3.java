package pl.edu.uwm.wmii.jurczykpatryk.labolatorium03;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Zadanie3 {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj wyraz: ");
        String pom = sc.nextLine();
        System.out.print("Podaj sciezke do pliku: ");
        String sciezka=sc.nextLine();
        System.out.println("Tyle jest "+"'"+pom+"'"+" : "+ileJestZnakow(sciezka,pom));
    }

    public static int ileJestZnakow(String name , String p) throws FileNotFoundException {
        Scanner file = new Scanner(new File(name));
        String plikclone="";
        while (file.hasNextLine()) {
            plikclone += file.nextLine();
        }
        int wynik=0;
        int pom=0;
        for(int i=0;i<=plikclone.length()-p.length();i++) {
            for(int j=0;j<p.length();j++) {
                if(plikclone.charAt(j+i)==p.charAt(j)) {
                    pom++;
                    if (pom == p.length()) {
                        wynik++;
                    }
                }
            }
            pom=0;
        }
        return wynik;
    }
}