package pl.edu.uwm.wmii.jurczykpatryk.labolatorium03;

import java.util.Scanner;

public class Zadanie1 {
    //A
    static int countChar(String str, char c){
        int rezultat=0;
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i)==c){
                rezultat++;
            }
        }
        return rezultat;
    }
    //B
    static int countSubStr(String str, String subStr){
        int rezultat=0;
        int pom=0;
        for(int i=0;i<=str.length()-subStr.length();i++) {
            for(int j=0;j<subStr.length();j++) {
                if(str.charAt(j+i)==subStr.charAt(j)) {
                    pom++;
                    if (pom == subStr.length()) {
                        rezultat++;
                    }
                }
            }
            pom=0;
        }
        return rezultat;
    }
    //C
    static String middle(String str){
        String rezultat = "";
        if(str.length()%2==0){
            rezultat+=str.charAt((str.length()/2)-1);
            rezultat+=str.charAt((str.length()/2));
        }
        else {
            rezultat+=str.charAt((str.length()/2));
        }
        return rezultat;
    }
    //D
    static String repeat(String str, int n){
        StringBuilder rezultat= new StringBuilder();
        String pom ="";
        for(int i=0; i<n; i++){
            rezultat.append(pom.concat(str));
        }
        return rezultat.toString();
    }
    //E
    static int[] where(String str, String subStr){
        int[] rezultat = new int[str.length()];
        int pom=0;
        int pom1 = 0;
        for(int i=0;i<=str.length()-subStr.length();i++) {
            for(int j=0;j<subStr.length();j++) {
                if(str.charAt(j+i)==subStr.charAt(j)) {
                    pom++;
                    if (pom == subStr.length()) {
                        rezultat[pom1] += i;
                        pom1++;
                    }
                }
            }
            pom=0;
        }
        return rezultat;
    }
    public static void wypisz(int[] tab) {
        for (int j : tab) {
            System.out.print(j + " ");
        }
        System.out.println();
    }
    //F
    static String change(String str){
        StringBuffer pom = new StringBuffer();
        pom.append(str);
        for(int i=0;i<pom.length();i++) {
            if(pom.charAt(i)>='a'&& pom.charAt(i)<='z'){
                pom.setCharAt(i,(char)(pom.charAt(i)+('A'-'a')));
                continue;
            }
            if(pom.charAt(i)>='A'&& pom.charAt(i)<='Z') {
                pom.setCharAt(i,(char)(pom.charAt(i)-('A'-'a')));
            }
        }
        return pom.toString();
    }
    //G
    public static String nice(String str) {
        StringBuffer pom=new StringBuffer();
        pom.append(str);
        int pom1=0;
        for(int i=3; i<str.length()+pom1; i+=4) {
            pom.insert((i), ',');
            pom1++;
        }
        return pom.toString();
    }
    //H
    public static String nice1(String str, int miesc, char znak) {
        StringBuffer pom=new StringBuffer();
        pom.append(str);
        int pom1=0;
        for(int i=miesc; i<str.length()+pom1; i+=(miesc+1)) {
            pom.insert((i), znak);
            pom1++;
        }
        return pom.toString();
    }


    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj tekst: ");
        String x = sc.nextLine();
        System.out.println("Podaj słowa szukanego w tekscie: ");
        String g = sc.nextLine();
        System.out.println("Podaj znak którgo szukasz: ");
        String pom = sc.nextLine();
        System.out.println("Podaj znak który chcesz wstawic: ");
        String pom1 = sc.nextLine();
        char c = pom.charAt(0);
        char f = pom1.charAt(0);
        System.out.println("Podaj liczbe: ");
        int n = sc.nextInt();
        System.out.println("Ilość wystąpien znaku: "+countChar(x,c));
        System.out.println("Ilośc wystapien wyrazu: "+countSubStr(x,g));
        System.out.println("Znak: "+middle(x));
        System.out.println("Wynik: "+repeat(g,n));
        int[] a = where(x,g);
        wypisz(a);
        System.out.println("Po zamianie: "+change(x));
        System.out.println("Wstawnianie: "+nice(x));
        System.out.println("Wstawnianie: "+nice1(x,n,f));
    }
}
