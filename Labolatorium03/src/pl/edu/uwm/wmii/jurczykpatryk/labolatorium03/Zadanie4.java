package pl.edu.uwm.wmii.jurczykpatryk.labolatorium03;

import java.math.BigInteger;
import java.util.Scanner;

public class Zadanie4 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(sumaSzachownica(n));
    }

    public static BigInteger sumaSzachownica(int n) {
        int wynik = 0;
        int x = 2;
        BigInteger rezutat=new BigInteger(Integer.toString(wynik));
        BigInteger pom=new BigInteger(Integer.toString(x));
        for(int i=0;i<n*n;i++) {
            rezutat=rezutat.add(pom.pow(i));
        }
        return rezutat;
    }
}