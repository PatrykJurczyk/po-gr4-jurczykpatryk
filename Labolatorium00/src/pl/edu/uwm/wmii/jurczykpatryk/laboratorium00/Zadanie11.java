package pl.edu.uwm.wmii.jurczykpatryk.laboratorium00;

public class Zadanie11 {
    public static void  main(String[] args){
        String wiersz="Trzy słowa najdziwniejsze\n" +
                "Kiedy wymawiam słowo Przyszłość,\n" +
                "pierwsza sylaba odchodzi już do przeszłości.\n" +
                "Kiedy wymawiam słowo Cisza,\n" +
                "niszczę ją.\n" +
                "Kiedy wymawiam słowo Nic,\n" +
                "stwarzam coś, co nie mieści się w żadnym niebycie";
        System.out.println(wiersz);
    }
}
