package pl.edu.uwm.wmii.jurczykpatryk.kolokwium00;

public class Zadanie2 {
    public static String delete(String str, char c){
        String pom = "";
        for(int i=0; i<str.length(); i++) {
            {
                pom = str.replaceAll("[aeiou]", String.valueOf(c));
            }
        }
        return pom;
    }

    public static void main(String[] args){
        String pom = "MatEmatyka";
        char c = 'r';
        System.out.println(delete(pom,c));
    }
}
