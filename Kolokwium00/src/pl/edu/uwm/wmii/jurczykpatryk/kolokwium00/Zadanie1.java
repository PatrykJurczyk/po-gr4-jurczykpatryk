package pl.edu.uwm.wmii.jurczykpatryk.kolokwium00;

import java.util.Scanner;

public class Zadanie1 {

    public static void main(String[] args) {
        int liczbyuj = 0;
        int liczbydod = 0;
        int liczbyrow = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj liczbe naturalną: ");
        Integer n = Integer.valueOf(in.nextInt());
        for (int i = 0; i < n; i++) {
            System.out.print("Podaj kolejna liczbę: ");
            Integer z = Integer.valueOf((in.nextInt()));
            if (z > 5) {
                liczbydod++;
            } else if (z < (-5)) {
                liczbyuj++;
            } else if (z == 5) {
                liczbyrow++;
            }
        }
        System.out.println("Jest "+liczbydod+" liczb wiekszych od 5.");
        System.out.println("Jest "+liczbyuj+" liczb mniejszych od -5.");
        System.out.println("Jest "+liczbyrow+" rownych 5.");
    }
}
