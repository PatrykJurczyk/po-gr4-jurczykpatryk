package pl.edu.uwm.wmii.jurczykpatryk.laboratorium10;

import java.util.LinkedList;
import java.util.Scanner;

public class Zadanie7 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n= s.nextInt();
        prime(n);
    }
    public static void prime(int x){
        LinkedList<Integer> first = new LinkedList<>();
        for (int i=2;i<=x;i++){
            first.add(i);
        }
        for (int j=2;j<=Math.sqrt(x);j++){
            for (int k=0;k<=first.size()-1;k++){
                if(first.get(k)%j==0&&first.get(k)!=j){
                    first.remove(k);
                }
            }
        }
        System.out.println(first);
    }
}