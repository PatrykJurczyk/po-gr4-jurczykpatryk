package pl.edu.uwm.wmii.jurczykpatryk.laboratorium10;

import java.util.LinkedList;

public class Zadanie3 {

    public static void main(String[] args) {
        LinkedList<String> lista = new LinkedList<>();
        lista.add("Kowalski");
        lista.add("Nowak");
        lista.add("Jurczyk");
        lista.add("Malinowski");
        lista.add("PanX");
        lista.add("PaniY");
        System.out.println(lista);
        odwroc(lista);
        System.out.println(lista);
    }
    public static void odwroc (LinkedList<String> pracownicy){
        LinkedList<String> pom= new LinkedList<>();
        for (int i=pracownicy.size()-1;i>=0;i--){
            pom.add(pracownicy.getLast());
            pracownicy.removeLast();
        }
        for (int j=pom.size()-1;j>=0;j--){
            pracownicy.add(pom.getFirst());
            pom.removeFirst();
        }
    }
}