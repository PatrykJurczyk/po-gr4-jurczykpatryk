package pl.edu.uwm.wmii.jurczykpatryk.laboratorium10;

import java.util.LinkedList;
import java.util.Stack;

public class Zadanie8 {

    public static void main(String[] args) {
        LinkedList<String> lista = new LinkedList<>();
        lista.add("Kowalski");
        lista.add("Nowak");
        lista.add("Jurczyk");
        lista.add("Malinowski");
        lista.add("PanX");
        lista.add("PaniY");
        print(lista);
        Stack<Integer> lista1 = new Stack<>();
        lista1.add(1);
        lista1.add(2);
        lista1.add(3);
        lista1.add(4);
        lista1.add(5);
        lista1.add(6);
        print (lista1);
    }
    public static <E> void print(Iterable<E> o){
        for( E i:o){
            System.out.print(i);
            System.out.print(",");
        }
    }
}