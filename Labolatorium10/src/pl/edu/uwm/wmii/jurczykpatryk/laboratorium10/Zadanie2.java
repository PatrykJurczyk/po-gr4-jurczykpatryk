package pl.edu.uwm.wmii.jurczykpatryk.laboratorium10;

import java.util.LinkedList;

public class Zadanie2 {

    public static void main(String[] args) {
        LinkedList<String> lista = new LinkedList<>();
        lista.add("Kowalski");
        lista.add("Nowak");
        lista.add("Jurczyk");
        lista.add("Malinowski");
        lista.add("PanX");
        lista.add("PaniY");
        System.out.println(lista);
        redukuj(lista,4);
        System.out.println(lista);

    }
    public static <T> void redukuj (LinkedList<T> pracownicy,int n) {
        LinkedList<T> pom = (LinkedList<T>) pracownicy.clone();
        int x = pracownicy.size();
        if (n < 2) {
            for (int i = 1; i <= x; i++) {
                if (n == 0) {
                    System.out.println("ERROR");
                    break;
                } else if (n == 1) {
                    pracownicy.remove(0);
                }
            }
        }else {
            pracownicy.clear();
            for (int i = 0; i <x; i++) {
                if ((i+1) % n != 0) {
                    pracownicy.add(pom.get(i));
                }
            }
        }
    }
}