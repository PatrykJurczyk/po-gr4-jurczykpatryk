package pl.edu.uwm.wmii.jurczykpatryk.laboratorium10;

import java.util.Scanner;
import java.util.Stack;

public class Zadanie6 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        int liczba = scan.nextInt();
        int liczba1=liczba;
        Stack<Integer> stack=new Stack<Integer>();
        int ilosccyfr = 0;
        while ( liczba1!=0 ){
            liczba1 = liczba1 / 10;
            ilosccyfr++;
        }
        for(int i=0; i<ilosccyfr; i++){
            int x = liczba % 10;
            liczba = liczba / 10;
            stack.push(x);
        }
        while (!stack.isEmpty()){
            int temp = stack.peek();
            System.out.print(temp + " ");
            stack.pop();
        }
    }
}
