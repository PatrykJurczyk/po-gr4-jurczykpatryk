package pl.edu.uwm.wmii.jurczykpatryk.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_g {
    public static void  main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Podaj ilosc generowanych liczb: ");
            int ilosc = scanner.nextInt();
            if (ilosc < 1 || ilosc > 100) {
                System.out.println("Podales zla liczbe!! Podaj z przedzialu 1-100 :) ");
            }
            else {
                int pocz = -999;
                int kon = 999;
                int [] zbiorliczb = new int[ilosc];
                while (true){
                    System.out.print("Podaj liczbe x: ");
                    int lewy = scanner.nextInt();
                    System.out.print("Podaj liczbe y wieksza od x: ");
                    int prawy = scanner.nextInt();
                    if(lewy<1 || lewy>prawy || prawy>ilosc){
                        System.out.println("Podales zle liczby Try again!!");
                        continue;
                    }
                    else {
                        for (int i = 0; i < ilosc; i++) {
                            zbiorliczb[i] = (randomBetween(pocz, kon));
                        }
                        for (int value : zbiorliczb) {
                            System.out.print(" " + value);
                        }
                        System.out.println();
                        int k = prawy - lewy + 1;
                        int [] pom = new int[k];
                        int j=0;
                        for (int i = lewy -1; i < prawy; i++) {
                            pom[j] = zbiorliczb[i];
                            j++;
                        }
                        System.out.println();
                        for(int i=(pom.length-1);i>=0;i--){
                            zbiorliczb[lewy-1] = pom[i];
                            lewy++;
                        }
                        System.out.println();
                        for (int value : zbiorliczb) {
                            System.out.print(" " + value);
                        }
                        System.out.println();
                    }
                    break;
                }
            }
            break;
        }
    }
    //Losowanie liczb od -> do;
    static int randomBetween ( int poczatek, int koniec) {
        Random random = new Random();
        int x1 = random.nextInt(koniec - poczatek + 1);
        int x2 = x1 + poczatek;
        return x2;
    }
}
