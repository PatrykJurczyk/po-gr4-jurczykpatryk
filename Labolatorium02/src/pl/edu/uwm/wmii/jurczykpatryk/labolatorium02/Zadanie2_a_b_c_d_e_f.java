package pl.edu.uwm.wmii.jurczykpatryk.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2_a_b_c_d_e_f {
    public static void generuj (int[] tab, int n, int min, int max){
        Random random = new Random();
        for(int i=0; i<n; i++){
            int x1 = random.nextInt(max - min + 1);
            tab[i] = x1 + min;
        }
    }
    public static int ileNieparzystych (int[] tab){
        int wynik = 0;
        for (int j : tab) {
            if (j % 2 == 1 || j % 2 == (-1)) {
                wynik++;
            }
        }
        return wynik;
    }
    public static int ileParzystych (int[] tab){
        int wynik = 0;
        for (int j : tab) {
            if (j % 2 == 0) {
                wynik++;
            }
        }
        return wynik;
    }
    public static int ileDodatnich (int[] tab){
        int wynik = 0;
        for (int j : tab) {
            if (j > 0) {
                wynik++;
            }
        }
        return wynik;
    }
    public static int ileUjemnych (int[] tab){
        int wynik=0;
        for (int j : tab) {
            if (j < 0) {
                wynik++;
            }
        }
        return wynik;
    }
    public static int ileZerowych (int[] tab){
        int wynik=0;
        for (int j : tab) {
            if (j == 0) {
                wynik++;
            }
        }
        return wynik;
    }
    public static int maksymana(int[] tab){
        int wynik=tab[0];
        for (int j : tab) {
            if (wynik < j) {
                wynik = j;
            }
        }
        return wynik;
    }
    public static int ileMaksymalnych (int[] tab){
        int max = maksymana(tab);
        int wynik=0;
        for (int j : tab) {
            if (j == max) {
                wynik++;
            }
        }
        return wynik;
    }
    public static int sumaDodatnich (int[] tab){
        int wynik=0;
        for (int j : tab) {
            if (j > 0) {
                wynik += j;
            }
        }
        return wynik;
    }
    public static int sumaUjemnych (int[] tab){
        int wynik=0;
        for (int j : tab) {
            if (j < 0) {
                wynik += j;
            }
        }
        return wynik;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich (int[] tab){
        int pom=0;
        int wynik=0;
        for(int i=0; i<tab.length-1;i++){
            int x = tab[i];
            int y = tab[(i+1)];
            if(x>0 && y>0){
                pom++;
                if(wynik<(pom+1)){
                    wynik=(pom+1);
                }
            }
            else {
                pom=0;
            }
        }
        return wynik;
    }
    public static void signum(int[] tab){
        int[] x = tab.clone();
        for(int i=0; i<tab.length;i++){
            int z = x[i];
            if(z<0){
                x[i]=(-1);
            }
            else {
                x[i]=1;
            }
        }
        for (int j : x) {
            System.out.print(j + " ");
        }
        System.out.println();
    }

    public static void  main(String[] args){
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Podaj ilosc generowanych liczb: ");
            Integer ilosc = scanner.nextInt();
            if (ilosc < 1 || ilosc > 100) {
                System.out.println("Podales zla liczbe!! Podaj z przedzialu 1-100 :) ");
            } else {
                int[] zbliczb = new int[ilosc];
                generuj(zbliczb, ilosc, -999, 999);
                System.out.println("Ilosc liczb dodatnich: " + ileDodatnich(zbliczb));
                System.out.println("Ilosc liczb ujemnych: " + ileUjemnych(zbliczb));
                System.out.println("Ilosc liczb nieparzystych: " + ileNieparzystych(zbliczb));
                System.out.println("Ilosc liczb parzystych: " + ileParzystych(zbliczb));
                System.out.println("Ilosc zer: " + ileZerowych(zbliczb));
                System.out.println("Najwieksza licza to: " + maksymana(zbliczb));
                System.out.println("Najwieksza liczba to: " + maksymana(zbliczb) + " ilosc pojawien: " + ileMaksymalnych(zbliczb));
                System.out.println("Suma liczb dodatnich: " + sumaDodatnich(zbliczb));
                System.out.println("Suma liczb ujemnych: " + sumaUjemnych(zbliczb));
                System.out.println("Najdluzszy ciag liczb dodatnich to: " + dlugoscMaksymalnegoCiaguDodatnich(zbliczb));
                signum(zbliczb);
                //Sprawdzenie czy działa
                //for(int i=0; i<zbliczb.length;i++){
                //    System.out.println(zbliczb[i]);
                //}
                break;
            }
        }
    }
}
