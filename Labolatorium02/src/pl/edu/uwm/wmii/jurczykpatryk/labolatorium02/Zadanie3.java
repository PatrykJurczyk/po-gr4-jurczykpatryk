package pl.edu.uwm.wmii.jurczykpatryk.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie3 {
    public static void mnozenie(int[][] m,int[][] n) {
        int[][] wynik=new int[m.length][n[0].length];
        if (n.length==m[0].length) {
            for (int i=0;i<wynik.length;i++) {
                for (int j=0;j<wynik[0].length;j++) {
                    wynik[i][j]=0;
                    for (int x=0;x<m[0].length;x++) {
                        wynik[i][j]=wynik[i][j]+m[i][x]*n[x][j];
                    }
                }
            }
        }
        else {
            wynik=null;
        }
        for(int i=0; i<m.length;i++){
            for (int j=0; j<n.length;j++){
                assert wynik != null;
                System.out.print(" "+wynik[i][j]);
            }
            System.out.println();
        }
    }

    public static void  main(String[] args){
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Podaj liczbe m: ");
            int m = scanner.nextInt();
            System.out.print("Podaj liczbe n: ");
            int n = scanner.nextInt();
            System.out.print("Podaj liczbe k: ");
            int k = scanner.nextInt();
            if ((m<1 || m>10)||(n<1 || n>10)||(k<1 || k>10)) {
                System.out.println("Podales zle liczby!! Podaj z przedzialu 1-10 :) ");
            }
            else {
                //Macierz A
                int pocz = 1;
                int kon = 10;
                int[][] tab = new int[m][n];
                for(int i=0; i<m; i++){
                    for(int j=0; j<n; j++){
                        tab[i][j] = (randomBetween(pocz, kon));
                    }
                }
                for(int i=0; i<m;i++){
                    for (int j=0; j<n;j++){
                        System.out.print(" "+tab[i][j]);
                    }
                    System.out.println();
                }
                System.out.println();
                //Macierz B
                int[][] tab1 = new int[n][k];
                for(int i=0; i<n; i++){
                    for(int j=0; j<k; j++){
                        tab1[i][j] = (randomBetween(pocz, kon));
                    }
                }
                for(int i=0; i<n;i++){
                    for (int j=0; j<k;j++){
                        System.out.print(" "+tab1[i][j]);
                    }
                    System.out.println();
                }
                System.out.println();
                //Macierz C
                mnozenie(tab,tab1);
            }
            break;
        }
    }
    static int randomBetween ( int poczatek, int koniec) {
        Random random = new Random();
        int x1 = random.nextInt(koniec - poczatek + 1);
        int x2 = x1 + poczatek;
        return x2;
    }
}
