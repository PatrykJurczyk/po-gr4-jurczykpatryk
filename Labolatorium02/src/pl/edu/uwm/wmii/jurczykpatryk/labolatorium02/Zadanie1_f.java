package pl.edu.uwm.wmii.jurczykpatryk.labolatorium02;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1_f {
    public static void  main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Podaj ilosc generowanych liczb: ");
            int ilosc = scanner.nextInt();
            if (ilosc < 1 || ilosc > 100) {
                System.out.println("Podales zla liczbe!! Podaj z przedzialu 1-100 :) ");
            }
            else {
                int pocz = -999;
                int kon = 999;
                ArrayList<Integer> zbiorliczb = new ArrayList<Integer>();
                for (int i = 0; i < ilosc; i++) {
                    zbiorliczb.add(randomBetween(pocz, kon));
                }
                for (int i = 0; i < zbiorliczb.size(); i++) {
                    int x = zbiorliczb.get(i);
                    if(x<0){
                        zbiorliczb.set(i,(-1));
                    }
                    else {
                        zbiorliczb.set(i,1);
                    }
                }
                for (int x : zbiorliczb) {
                    System.out.println(x);
                }
                break;
            }
        }
    }
    //Losowanie liczb od -> do;
    static int randomBetween ( int poczatek, int koniec) {
        Random random = new Random();
        int x1 = random.nextInt(koniec - poczatek + 1);
        int x2 = x1 + poczatek;
        return x2;
    }
}
