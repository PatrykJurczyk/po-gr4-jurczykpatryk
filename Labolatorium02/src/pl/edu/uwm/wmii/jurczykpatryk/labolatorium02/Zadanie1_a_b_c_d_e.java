package pl.edu.uwm.wmii.jurczykpatryk.labolatorium02;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1_a_b_c_d_e {

    public static void  main(String[] args){
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.print("Podaj ilosc generowanych liczb: ");
            int ilosc = scanner.nextInt();
            if (ilosc < 1 || ilosc > 100) {
                System.out.println("Podales zla liczbe!! Podaj z przedzialu 1-100 :) ");
            }
            else {
                //System.out.print("Podaj poczatek przedzialu losowania: ");
                //Integer pocz = scanner.nextInt();
                //System.out.print("Podaj koniec przedzialu losowania: ");
                //Integer kon = scanner.nextInt();
                int pocz = -999;
                int kon = 999;
                ArrayList<Integer> zbiorliczb = new ArrayList<Integer>();

                for (int i = 0; i < ilosc; i++) {
                    zbiorliczb.add(randomBetween(pocz, kon));
                }
                int parzyst = 0;
                int nparzyst = 0;
                int uj = 0;
                int dod = 0;
                int zer = 0;
                int max = zbiorliczb.get(0);
                int ilmax = 0;
                int sumaujem = 0;
                int sumadodat = 0;
                int ciag = 0;
                for (int x : zbiorliczb) {
                    //System.out.println(": " + x);
                    //Podpunkt a
                    if (x % 2 == 0) {
                        parzyst++;
                    } else {
                        nparzyst++;
                    }
                    //Podpunkt b i d
                    if (x > 0) {
                        dod++;
                        sumadodat += x;
                    } else if (x < 0) {
                        uj++;
                        sumaujem += x;
                    } else {
                        zer++;
                    }
                    //Podpunkt c
                    if (max < x) {
                        max = x;
                    }
                }
                System.out.println("Ilosc liczb parzystych: " + parzyst);
                System.out.println("Ilosc liczb nieparzystych: " + nparzyst);
                System.out.println("Ilosc liczb ujemnych: " + uj);
                System.out.println("Ilosc liczb dodatnich: " + dod);
                System.out.println("Ilosc zer: " + zer);
                System.out.println("Najwieksza licza to: " + max);
                System.out.println("Suma liczb ujemnych: " + sumaujem);
                System.out.println("Suma liczb dodatnich: " + sumadodat);
                for (int z : zbiorliczb) {
                    //Podpunkt c dalsza czesc;
                    if (z == max) {
                        ilmax++;
                    }
                }
                System.out.println("Najwieksza liczba to: " + max + " ilosc pojawien: " + ilmax);
                //Podpunkt e
                int pom = 0;
                for (int k = 0; k < (zbiorliczb.size() - 1); k++) {
                    int z = zbiorliczb.get(k);
                    int x = zbiorliczb.get(k + 1);
                    if (z > 0 && x > 0) {
                        pom++;
                        if (ciag < (pom + 1)) {
                            ciag = (pom + 1);
                        }
                    } else {
                        pom = 0;
                    }
                }
                System.out.println("Najdluzszy ciag liczb dodatnich to: " + ciag);
                break;
            }
        }
    }
    //Losowanie liczb od -> do;
    static int randomBetween ( int poczatek, int koniec) {
        Random random = new Random();
        int x1 = random.nextInt(koniec - poczatek + 1);
        int x2 = x1 + poczatek;
        return x2;
    }
}
