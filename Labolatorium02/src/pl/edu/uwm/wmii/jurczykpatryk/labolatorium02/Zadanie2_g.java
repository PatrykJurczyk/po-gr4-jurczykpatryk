package pl.edu.uwm.wmii.jurczykpatryk.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2_g {
    public static void generuj(int[] tab, int n, int min, int max) {
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            int x1 = random.nextInt(max - min + 1);
            tab[i] = (int) (x1 + min);
        }
    }

    public static void odwrocFragment(int[] tab, int lewy, int prawy) {
        for (int value : tab) {
            System.out.print(" " + value);
        }
        int k = prawy - lewy + 1;
        int[] pom = new int[k];
        int j = 0;
        for (int i = lewy - 1; i < prawy; i++) {
            pom[j] = tab[i];
            j++;
        }
        System.out.println();
        for (int i = (pom.length - 1); i >= 0; i--) {
            tab[lewy - 1] = pom[i];
            lewy++;
        }
        System.out.println();
        for (int value : tab) {
            System.out.print(" " + value);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Podaj ilosc generowanych liczb: ");
            int ilosc = scanner.nextInt();
            if (ilosc < 1 || ilosc > 100) {
                System.out.println("Podales zla liczbe!! Podaj z przedzialu 1-100 :) ");
            } else {
                while (true) {
                    System.out.print("Podaj liczbe x: ");
                    int lewy = scanner.nextInt();
                    System.out.print("Podaj liczbe y wieksza od x: ");
                    int prawy = scanner.nextInt();
                    if (lewy < 1 || lewy > prawy || prawy > ilosc) {
                        System.out.println("Podales zle liczby Try again!!");
                        continue;
                    } else {
                        int[] zbliczb = new int[ilosc];
                        generuj(zbliczb, ilosc, -999, 999);
                        odwrocFragment(zbliczb, lewy, prawy);
                    }
                    break;
                }
            }
            break;
        }
    }
}
