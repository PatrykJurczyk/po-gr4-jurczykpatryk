package pl.imiajd.jurczyk;

import java.time.LocalDate;

public abstract class Osoba {
    public Osoba(String nazwisko,
                 String[] imiona,
                 LocalDate dataUrodzenia,
                 boolean plec) {
        this.dataUrodzenia = dataUrodzenia;
        this.imiona = imiona;
        this.plec = plec;
        this.nazwisko = nazwisko;
    }

    public abstract String getOpis();


    public String getNazwisko() {
        return nazwisko;
    }
    public String[] getImiona() {
        return imiona;
    }
    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }
    public boolean getPlec() {
        return plec;
    }
    private String nazwisko;
    String[] imiona;
    LocalDate dataUrodzenia;
    boolean plec;
}
