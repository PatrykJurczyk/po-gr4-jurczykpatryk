package pl.imiajd.jurczyk;

import java.time.LocalDate;

public class Skrzypce extends Instrument{

    public Skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }


    public String dzwięk() {
        return "Brzmeinie skrzypiec";
    }

    public String toString(){
        return "Skrzypce - " +" tworca: "+ getProducent() +", data-produkcji: "+getRokProdukcji();
    }
}
