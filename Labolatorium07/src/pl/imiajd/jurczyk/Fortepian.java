package pl.imiajd.jurczyk;

import java.time.LocalDate;

public class Fortepian extends Instrument{

    public Fortepian(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }


    public String dzwięk() {
        return "Brzmeinie fortepianu";
    }

    public String toString(){
        return "Fortepian - " +" tworca: "+ getProducent() +", data-produkcji: "+getRokProdukcji();
    }
}
