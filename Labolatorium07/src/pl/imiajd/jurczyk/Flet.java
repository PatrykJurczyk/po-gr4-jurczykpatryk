package pl.imiajd.jurczyk;

import java.time.LocalDate;

public class Flet  extends Instrument{

    public Flet(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }


    public String dzwięk() {
        return "Brzmeinie fleta";
    }

    public String toString(){
        return "Flet - " +" tworca: "+ getProducent() +", data-produkcji: "+getRokProdukcji();
    }
}