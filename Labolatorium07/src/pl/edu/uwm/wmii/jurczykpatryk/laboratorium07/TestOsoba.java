package pl.edu.uwm.wmii.jurczykpatryk.laboratorium07;

import java.time.LocalDate;
import java.util.*;
import pl.imiajd.jurczyk.*;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];
        ludzie[0] = new Pracownik("Kimono", new String[]{"Paweł Marek"},
                LocalDate.of(2000,04,01),true,
                50000.00,LocalDate.of(2015,03,02));
        ludzie[1] = new Pracownik("Kowalski", new String[]{"Ala Ola"},
                LocalDate.of(1987,12,29),false,
                56300.00,LocalDate.of(2012,03,22));

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko()+" "+Arrays.toString(p.getImiona())+" "+p.getDataUrodzenia()+" "+p.getPlec()+" "+p.getOpis());
        }
    }
}








