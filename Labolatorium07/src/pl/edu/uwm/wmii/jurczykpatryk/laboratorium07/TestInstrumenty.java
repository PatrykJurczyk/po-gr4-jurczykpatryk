package pl.edu.uwm.wmii.jurczykpatryk.laboratorium07;

import pl.imiajd.jurczyk.Flet;
import pl.imiajd.jurczyk.Fortepian;
import pl.imiajd.jurczyk.Instrument;
import pl.imiajd.jurczyk.Skrzypce;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {

    public static void main(String[] args) {

        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(2010,04,02)));
        orkiestra.add(new Fortepian("Yamaha", LocalDate.of(2010,05,23)));
        orkiestra.add(new Flet("Yamaha", LocalDate.of(2010,05,20)));
        orkiestra.add(new Flet("Yamaha", LocalDate.of(2008,01,16)));
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(2018,05,19)));

        for(int i=0;i<orkiestra.size();i++){
            System.out.println(orkiestra.get(i).dzwięk());
        }

        for(int j=0;j<orkiestra.size();j++){
            System.out.println(orkiestra.get(j).toString());
        }
    }
}