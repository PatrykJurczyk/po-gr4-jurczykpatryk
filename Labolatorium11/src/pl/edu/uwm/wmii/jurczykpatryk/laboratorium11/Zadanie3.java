package pl.edu.uwm.wmii.jurczykpatryk.laboratorium11;

import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

class Zadanie3 implements Comparable<Zadanie3>{
    String imie;
    String nazwisko;
    int id=0;
    public Zadanie3(String imie,String nazwisko,int id){
        this.id = id;
        this.imie = imie;
        this.nazwisko=nazwisko;
    }

    public int getId(){

        return this.id;
    }

    @Override
    public String toString() {

        return "Nazwisko: "+ this.nazwisko + " Imie: "+ this.imie + " ID: "+ this.id;
    }

    @Override
    public int compareTo(Zadanie3 o) {
        int result = this.nazwisko.compareTo(o.nazwisko);
        if(result==0){
            result=this.imie.compareTo(o.imie);
        }
        if(result==0){
            result = Integer.compare(this.id,o.id);
        }
        return result;
    }
}

class Main1 {
    public static void sortedArray(HashMap<Zadanie3,String> x){
        TreeMap<Zadanie3,String> s = new TreeMap<>();
        s.putAll(x);
        s.entrySet().forEach(entry->{
            System.out.println(entry.getKey() + " Ocena: " + entry.getValue());
        });
    }
    public static void main(String[] args) {
        HashMap<Zadanie3,String> Std = new HashMap<>();
        Zadanie3 X = new Zadanie3("Pat","Jurcz",231);
        Zadanie3 Y = new Zadanie3("Y","Adasfsa",234);
        Zadanie3 Z= new Zadanie3("Z","Afdsa",654);
        Zadanie3 A= new Zadanie3("A","Fjgdgh",246);
        Zadanie3 B = new Zadanie3("B","Fjkilo",634);
        Std.put(X,"db");
        Std.put(Y,"bdb");
        Std.put(Z,"dst");
        Std.put(A,"ndst");
        Std.put(B,"db");
        while (true){
            Scanner scan = new Scanner(System.in);
            String x= scan.nextLine();
            HashMap<Zadanie3,String> pomoc = new HashMap<>();
            if (x.equals("dodaj")){
                System.out.println("Kogo chcesz dodac");
                String y= scan.nextLine();
                String [] splitted = y.split("\\s+");
                Std.put(new Zadanie3(splitted[0],splitted[1],Integer.parseInt(splitted[2])),splitted[3]);
                pomoc.put(new Zadanie3(splitted[0],splitted[1],Integer.parseInt(splitted[2])),splitted[3]);
            }
            if(x.equals("usun")){
                System.out.println("Kogo chcesz usunac?");
                String z= scan.nextLine();
                int identyfikator = Integer.parseInt(z);
                Zadanie3 s = null;
                for(Zadanie3 k : Std.keySet()){
                    if(k.getId()==identyfikator){
                        s=k;
                        break;
                    }
                }
                if(s!=null){
                    Std.remove(s);
                }
            }
            if (x.equals("aktualizuj")){
                String e= scan.nextLine();
                String [] splitted = e.split("\\s+");
                int identyfikator = Integer.parseInt(splitted[0]);
                Zadanie3 s = null;
                for(Zadanie3 k : Std.keySet()){
                    if(k.getId()== identyfikator){
                        s=k;
                        break;
                    }

                }
                Std.replace(s,splitted[1]);
            }
            if(x.equals("wypisz")){
                sortedArray(Std);
            }
            if (x.equals("zakoncz")){
                break;
            }
            else{
                System.out.println("Nie rozpoznano polecenia!!");
            }
        }
    }
}