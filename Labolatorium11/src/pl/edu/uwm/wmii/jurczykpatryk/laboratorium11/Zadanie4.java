package pl.edu.uwm.wmii.jurczykpatryk.laboratorium11;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class Zadanie4 {
    public Zadanie4(String p) throws FileNotFoundException {
        this.kody= new HashMap<>();
        File pl = new File(p);
        Scanner s= new Scanner(pl);
        while (s.hasNext()){
            String word= s.next();
            boolean czy=false;
            for(int x: this.kody.keySet()){
                if (word.hashCode() == x) {
                    czy = true;
                    break;
                }
            }
            if(!czy){
                this.kody.put(word.hashCode(),new HashSet<>());
            }
            this.kody.get(word.hashCode()).add(word);
        }
        this.wypisz();
    }

    public void wypisz(){
        for(int x: this.kody.keySet()){
            if(this.kody.get(x).size()>1){
                System.out.print(x+":");
                for(String word: this.kody.get(x)){
                    System.out.print(" "+word);
                }
                System.out.println();
            }
        }
    }

    private HashMap<Integer, HashSet<String>> kody;

    public static void main(String[] args) throws FileNotFoundException {
        Zadanie4 fm= new Zadanie4("123.txt");
    }
}