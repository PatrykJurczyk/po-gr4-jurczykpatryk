package pl.edu.uwm.wmii.jurczykpatryk.labolatorium05;

public class MainPracownikDemo1 {
    public static void main(String[] args){
        PracownikDemo1[] personel = new PracownikDemo1[3];

        // wypełnij tablicę danymi pracowników
        personel[0] = new PracownikDemo1("Karol Cracker", 75000, 2001, 12, 15);
        personel[1] = new PracownikDemo1("Henryk Hacker", 50000, 2003, 10, 1);
        personel[2] = new PracownikDemo1("Antoni Tester", 40000, 2005, 3, 15);

        // zwiększ pobory każdego pracownika o 20%
        for (PracownikDemo1 e : personel) {
            e.zwiekszPobory(20);
        }

        // wypisz informacje o każdym pracowniku
        for (PracownikDemo1 e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = PracownikDemo1.getNextId(); // wywołanie metody statycznej
        System.out.println("Następny dostępny id = " + n);
    }
}
