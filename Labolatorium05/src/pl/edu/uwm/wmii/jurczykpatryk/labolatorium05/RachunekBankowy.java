package pl.edu.uwm.wmii.jurczykpatryk.labolatorium05;

public class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo;

    public void ustawSaldo(double saldo){
        this.saldo = saldo;
    }
    public void obliczMiesiezcneOdsetki(){
        this.saldo += (this.saldo * rocznaStopaProcentowa)/12;
    }
    static void rocznaStopaProcentowa(double x){
        rocznaStopaProcentowa = x;
    }
    public double pobierzSaldo(){
        return this.saldo;
    }
}
