package pl.edu.uwm.wmii.jurczykpatryk.labolatorium05;

public class MainIntegerSet {
    public static void main(String[] args){
        IntegerSet x=new IntegerSet();
        IntegerSet y=new IntegerSet();
        x.insertElement(6);
        x.insertElement(9);
        x.insertElement(5);
        y.insertElement(8);
        y.insertElement(12);
        y.insertElement(4);
        System.out.println(IntegerSet.union(x,y));
        System.out.println(IntegerSet.intersection(x,y));
        System.out.println(x.equals(y));
        y.deleteElement(91);
        y.insertElement(7);
        System.out.println(x.equals(y));
    }
}
