package pl.edu.uwm.wmii.jurczykpatryk.labolatorium05;

public class MainRachunekBankowy {
    public static void main(String[] args){
        RachunekBankowy saver1 = new RachunekBankowy();
        RachunekBankowy saver2 = new RachunekBankowy();
        saver1.ustawSaldo(2000.0);
        saver2.ustawSaldo(3000.0);
        RachunekBankowy.rocznaStopaProcentowa(0.04);
        saver1.obliczMiesiezcneOdsetki();
        saver2.obliczMiesiezcneOdsetki();
        System.out.println(saver1.pobierzSaldo());
        System.out.println(saver2.pobierzSaldo());
        RachunekBankowy.rocznaStopaProcentowa(0.05);
        saver1.obliczMiesiezcneOdsetki();
        saver2.obliczMiesiezcneOdsetki();
        System.out.println(saver1.pobierzSaldo());
        System.out.println(saver2.pobierzSaldo());
    }
}
