package pl.edu.uwm.wmii.jurczykpatryk.labolatorium05;

public class IntegerSet {
    private boolean[] table;

    public IntegerSet() {
        this.table = new boolean[100];
    }

    public static IntegerSet union(IntegerSet x1, IntegerSet x2) {
        IntegerSet wynik = new IntegerSet();
        for (int i = 0; i < wynik.table.length; i++) {
            if (x1.table[i] == true || x2.table[i] == true) {
                wynik.table[i] = true;
            } else {
                wynik.table[i] = false;
            }
        }
        return wynik;
    }
    public static IntegerSet intersection(IntegerSet x1, IntegerSet x2){
        IntegerSet wynik = new IntegerSet();
        for(int i=0; i<wynik.table.length; i++){
            if(x1.table[i] == true && x2.table[i] == true){
                wynik.table[i] = true;
            }
            else{
                wynik.table[i] = false;
            }
        }
        return wynik;
    }
    public void insertElement(int x){
        this.table[x-1] = true;
    }
    public void deleteElement(int x){
        this.table[x-1] = false;
    }

    public String toString()
    {
        StringBuffer numbers=new StringBuffer();
        for(int i=0; i<numbers.toString().length(); i++)
        {
            if(this.table[i])
            {
                numbers.append(i+1);
                numbers.append(" ");
            }
        }
        return numbers.toString();
    }
    public boolean equals(IntegerSet a) {

        return this.toString().equals(a.toString());
    }
}
