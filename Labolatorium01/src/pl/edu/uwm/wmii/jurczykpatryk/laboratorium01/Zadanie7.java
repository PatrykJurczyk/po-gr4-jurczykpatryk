package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.Scanner;

public class Zadanie7 {
    public static void main(String[] args){
        double suma=0; // dla dodawania
        double suma1=1; //dla mnożenia
        System.out.println("Podaj liczbe: ");
        Scanner scanner = new Scanner(System.in);
        Integer liczba = Integer.valueOf((scanner.nextInt()));
        for(int i=0; i<liczba; i++){
            System.out.println("Podaj liczbe numer: "+(i+1));
            Double liczba1 = Double.valueOf((scanner.nextDouble()));
            suma += liczba1;
            suma1 *= liczba1;
        }
        System.out.println("Suma jest rowna: "+suma);
        System.out.println("Iloczyn jest rowny: "+suma1);
    }
}
