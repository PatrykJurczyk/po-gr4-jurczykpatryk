package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie22 {
    public static void main(String[] args) {
        int wynik=0;
        Scanner qw = new Scanner(System.in);
        System.out.print("Podaj ilosc wprowadanych liczb: ");
        Integer n = Integer.valueOf((qw.nextInt()));
        ArrayList<Integer> zbliczb = new ArrayList<Integer>();
        for (int i = 1; i <= n; i++) {
            System.out.print("Twoja liczba to: ");
            Integer k = Integer.valueOf((qw.nextInt()));
            zbliczb.add(k);
        }
        for(int i=0; i<zbliczb.size()-1; i++){
            if((zbliczb.get(i))>0 && (zbliczb.get(i+1))>0){
                wynik++;
                System.out.println("Para liczb to: "+zbliczb.get(i)+" "+zbliczb.get(i+1));
            }
        }
        System.out.println("Wynik jest rowny: "+wynik);
    }
}