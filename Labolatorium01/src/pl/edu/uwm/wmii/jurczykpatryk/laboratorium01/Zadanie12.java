package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie12 {
    public static void main(String[] args){
        int wynik=0;
        ArrayList<Integer> zbliczb = new ArrayList<Integer>();
        System.out.print("Podaj liczbe taka ile chcesz wpisac cyfr do listy: ");
        Scanner scanner = new Scanner(System.in);
        Integer x = Integer.valueOf((scanner.nextInt()));
        for(int i=0; i<x; i++){
            System.out.print("Twoja liczba to: ");
            Integer z = Integer.valueOf((scanner.nextInt()));
            zbliczb.add(z);
        }
        for(int i=0; i<x; i++){
            int c = zbliczb.get(i);
            if(c%3==0 && c%5==0){
                wynik++;
            }
            else{
                continue;
            }
        }
        System.out.println("Jest "+wynik+" liczb podzielnych przez 5 i 3.");
    }
}
