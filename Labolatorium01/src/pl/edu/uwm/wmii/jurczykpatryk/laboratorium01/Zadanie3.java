package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args){
        double suma=0;
        Scanner in = new Scanner(System.in);
        Integer liczba = Integer.valueOf((in.nextInt()));
        for(int i=0; i<liczba; i++) {
            Double liczba1 = Double.valueOf((in.nextDouble()));
            if (liczba1 >= 0) {
                suma += liczba1;
            }
            else {
                liczba1 *= (-1);
                suma += liczba1;
            }
        }
        System.out.println(suma);
    }
}
