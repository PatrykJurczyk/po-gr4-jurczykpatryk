package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie16 {
    public static void main(String[] args){
        int wynik=0;
        Scanner qw = new Scanner(System.in);
        System.out.print("Podaj ilosc wprowadanych liczb: ");
        Integer n = Integer.valueOf((qw.nextInt()));
        ArrayList<Integer> zbliczb = new ArrayList<Integer>();
        for(int i=1; i<=n; i++){
            System.out.print("Twoja liczba to: ");
            Integer k = Integer.valueOf((qw.nextInt()));
            zbliczb.add(k);
        }
        for(int i=1; i<=n; i++){
            if((i)%2==1 && zbliczb.get(i-1)%2==0){
                    System.out.println("Liczba spelniajaca warunek to: "+i+" : "+zbliczb.get(i-1));
                    wynik++;
                }
            }
        System.out.println("Wynik jest rowny: "+wynik);
    }
}
