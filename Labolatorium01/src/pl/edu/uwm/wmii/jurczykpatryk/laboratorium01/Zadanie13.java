package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie13 {
    public static void main(String[] args){
        int wynik=0;
        Scanner fx = new Scanner(System.in);
        System.out.print("Podaj ilosc wprowadanych liczb: ");
        Integer x = Integer.valueOf((fx.nextInt()));
        ArrayList<Integer> zbliczb = new ArrayList<Integer>();
        for(int i=0; i<x; i++){
            System.out.print("Twoja liczba to: ");
            Integer z = Integer.valueOf((fx.nextInt()));
            zbliczb.add(z);
        }
        for(int i=0; i<zbliczb.size(); i++){
            int c = zbliczb.get(i);
            if(Math.pow(c,0.5)%2==0){
                wynik++;
            }
            else {
                continue;
            }
        }
        System.out.println("Wynik jest rowny: "+wynik);
    }
}
