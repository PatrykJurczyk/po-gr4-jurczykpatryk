package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args){
        double suma=1;
        Scanner in = new Scanner(System.in);
        Integer n = Integer.valueOf(in.nextInt());
        for(int i=0; i<n; i++){
            Double z = Double.valueOf((in.nextDouble()));
            suma*=z;
        }
        System.out.println(suma);
    }
}
