package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.Scanner;

public class Zadanie9 {
    public static int silnia (int n){
        int iloczyn = 1;
        for(int i=1; i<=n; i++){
            iloczyn *= i;
        }
        return iloczyn;
    }
    public static void main(String[] args){
        double suma=0;
        Scanner scanner = new Scanner(System.in);
        Integer liczba = Integer.valueOf((scanner.nextInt()));
        for(int i=1; i<=liczba; i++){
            Double liczba1 = Double.valueOf((scanner.nextDouble()));
            if(i%2==1){
                liczba1 = liczba1*(-1);
                suma+=liczba1/silnia(liczba);
            }
            else {
                suma += liczba1 / silnia(liczba);
            }
        }
        System.out.format("Wynik jest rowny: %.2f%n",suma);
    }
}
