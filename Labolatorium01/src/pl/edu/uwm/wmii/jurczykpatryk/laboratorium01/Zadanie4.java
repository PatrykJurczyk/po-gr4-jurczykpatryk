package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.Scanner;
import java.lang.Math;

public class Zadanie4 {
    public static void main(String[] args){
        double suma=0;
        Scanner scanner = new Scanner(System.in);
        Integer liczba = Integer.valueOf((scanner.nextInt()));
        for(int i=0; i<liczba; i++){
            Double liczba1 = Double.valueOf((scanner.nextDouble()));
            if(liczba1>0){
                liczba1 = Math.pow(liczba1,0.5);
                suma+=liczba1;
            }
            else{
                liczba1 = liczba1*(-1);
                liczba1 = Math.pow(liczba1,0.5);
                suma+=liczba1;
            }
        }
        System.out.println(suma);
    }
}
