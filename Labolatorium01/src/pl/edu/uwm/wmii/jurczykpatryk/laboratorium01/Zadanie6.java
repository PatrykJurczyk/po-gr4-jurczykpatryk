package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.Scanner;
import java.lang.Math;

public class Zadanie6 {
    public static void main(String[] args){
        double suma=0;
        Scanner scanner = new Scanner(System.in);
        Integer liczba = Integer.valueOf((scanner.nextInt()));
        for(int i=0; i<liczba; i++){
            Double liczba1 = Double.valueOf((scanner.nextDouble()));
            liczba1 = Math.pow(liczba1,2);
            suma+=liczba1;
        }
        System.out.println(suma);
    }
}
