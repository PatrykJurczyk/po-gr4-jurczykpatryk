package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.Scanner;

public class Zadanie11 {
    public static void main(String[] args){
        int suma=0;
        Scanner scanner = new Scanner(System.in);
        Integer n = Integer.valueOf((scanner.nextInt()));
        if(n<0){
            System.out.println("Podales zla liczbe!!");
        }
        else{
            for(int i=0; i<n; i++){
                Integer x = Integer.valueOf((scanner.nextInt()));
                if(x%2==1){
                    suma++;
                }
                else{
                    continue;
                }
            }
            System.out.println(suma);
        }
    }
}
