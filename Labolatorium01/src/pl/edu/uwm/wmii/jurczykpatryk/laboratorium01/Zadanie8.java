package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args){
        double suma=0;
        Scanner scanner = new Scanner(System.in);
        Integer liczba = Integer.valueOf((scanner.nextInt()));
        for(int i=1; i<=liczba; i++){
            Double liczba1 = Double.valueOf((scanner.nextDouble()));
            if(i%2 == 1){
                suma+=liczba1;
            }
            else{
                suma-=liczba1;
            }
        }
        System.out.println("Wynik jest rowny: "+suma);
    }
}
