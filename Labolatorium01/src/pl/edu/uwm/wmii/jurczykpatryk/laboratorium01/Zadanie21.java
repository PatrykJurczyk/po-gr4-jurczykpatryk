package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie21 {
    public static void main(String[] args) {
        Scanner qw = new Scanner(System.in);
        System.out.print("Podaj ilosc wprowadanych liczb: ");
        Integer n = Integer.valueOf((qw.nextInt()));
        ArrayList<Integer> zbliczb = new ArrayList<Integer>();
        for (int i = 1; i <= n; i++) {
            System.out.print("Twoja liczba to: ");
            Integer k = Integer.valueOf((qw.nextInt()));
            zbliczb.add(k);
        }
        int min = zbliczb.get(0);
        int max = zbliczb.get(0);
        for(int i=0; i<zbliczb.size(); i++){
            if(zbliczb.get(i)<min){
                min = zbliczb.get(i);
            }
            if(zbliczb.get(i)>max){
                max = zbliczb.get(i);
            }
        }
        System.out.println("Najwieksza to: "+max+" najmniejsza to: "+min);
    }
}