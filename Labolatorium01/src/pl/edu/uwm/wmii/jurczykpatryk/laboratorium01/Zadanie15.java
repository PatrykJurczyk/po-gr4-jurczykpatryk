package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.ArrayList;
import java.util.Scanner;
import java.lang.Math;

public class Zadanie15 {
    public static int silnia (int n){
        int iloczyn = 1;
        for(int i=1; i<=n; i++){
            iloczyn *= i;
        }
        return iloczyn;
    }
    public static void main(String[] args){
        int wynik=0;
        Scanner qw = new Scanner(System.in);
        System.out.print("Podaj ilosc wprowadanych liczb: ");
        Integer n = Integer.valueOf((qw.nextInt()));
        ArrayList<Integer> zbliczb = new ArrayList<Integer>();
        for(int i=0; i<n; i++){
            System.out.print("Twoja liczba to: ");
            Integer k = Integer.valueOf((qw.nextInt()));
            zbliczb.add(k);
        }
        for(int i=0; i<zbliczb.size(); i++){
            if(i>=0 && i<n){
                double q = Math.pow(2,i);
                int w = silnia(i);
                if(zbliczb.get(i)>q && zbliczb.get(i)<w){
                    System.out.println(q+"<"+zbliczb.get(i)+"<"+w);
                    wynik++;
                    }
                }
            }
        System.out.println("Wynik jest rowny: "+wynik);
    }
}
