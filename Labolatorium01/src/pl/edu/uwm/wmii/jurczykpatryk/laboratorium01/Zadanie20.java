package pl.edu.uwm.wmii.jurczykpatryk.laboratorium01;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie20 {
    public static void main(String[] args) {
        int zera = 0;
        int plus = 0;
        int minus = 0;
        Scanner qw = new Scanner(System.in);
        System.out.print("Podaj ilosc wprowadanych liczb: ");
        Integer n = Integer.valueOf((qw.nextInt()));
        ArrayList<Integer> zbliczb = new ArrayList<Integer>();
        for (int i = 1; i <= n; i++) {
            System.out.print("Twoja liczba to: ");
            Integer k = Integer.valueOf((qw.nextInt()));
            zbliczb.add(k);
        }
        for (int i = 0; i < n; i++) {
            if ((zbliczb.get(i)) > 0) {
                plus++;
            }
            else if(zbliczb.get(i)<0){
                minus++;
            }
            else{
                zera++;
            }
        }
        System.out.println("Dodatnich jest: "+plus+" ujemnych jest: "+minus+" zer jest: "+zera);
    }
}