package pl.edu.uwm.wmii.jurczykpatryk.labolatorium04;

import java.util.ArrayList;

public class Zadanie2 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        wypisz(merge(a,b));
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<>();
        ArrayList<Integer> pom = (ArrayList<Integer>) a.clone();
        ArrayList<Integer> pomz = (ArrayList<Integer>) b.clone();
        int pom1=0;
        int pom2=0;
        for(int i=0;i<a.size()+b.size();i++){
            if(i%2==0){
                if(pom.isEmpty()){
                    wynik.add(b.get(pom2));
                    pom2++;
                }
                else {
                    wynik.add(a.get(pom1));
                    pom.remove(0);
                    pom1++;
                }

            }
            else {
                if(pomz.isEmpty()){
                    wynik.add(a.get(pom1));
                    pom1++;
                }
                else{
                    wynik.add(b.get(pom2));
                    pomz.remove(0);
                    pom2++;
                }

            }
        }
        return wynik;
    }
    public static void wypisz(ArrayList<Integer> wynik) {
        for (int i=0; i<wynik.size();i++) {
            System.out.print(wynik.get(i) + " ");
        }
        System.out.println();
    }
}
