package pl.edu.uwm.wmii.jurczykpatryk.labolatorium04;

import java.util.ArrayList;

public class Zadanie4 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        wypisz(reversed(a));
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> wynik = new ArrayList<>();
        for(int i=a.size()-1; i>=0;i--){
            wynik.add(a.get(i));
        }
        return wynik;
    }

    public static void wypisz(ArrayList<Integer> wynik) {
        for (int i=0; i<wynik.size();i++) {
            System.out.print(wynik.get(i) + " ");
        }
        System.out.println();
    }
}