package pl.edu.uwm.wmii.jurczykpatryk.labolatorium04;

import java.util.ArrayList;

public class Zadanie5 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        reversed(a);
        wypisz(a);
    }

    public static void reversed(ArrayList<Integer> a){
        ArrayList<Integer> pom = (ArrayList<Integer>) a.clone();
        a.clear();
        for(int i=pom.size()-1; i>=0;i--){
            a.add(pom.get(i));
        }
    }
    public static void wypisz(ArrayList<Integer> wynik) {
        for (int i=0; i<wynik.size();i++) {
            System.out.print(wynik.get(i) + " ");
        }
        System.out.println();
    }
}