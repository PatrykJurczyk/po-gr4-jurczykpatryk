package pl.edu.uwm.wmii.jurczykpatryk.labolatorium04;

import java.util.ArrayList;

public class Zadanie1 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        wypisz(append(a,b));
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<>();
        wynik.addAll(a);
        wynik.addAll(b);
        return wynik;
    }
    public static void wypisz(ArrayList<Integer> wynik) {
        for (int i=0; i<wynik.size();i++) {
            System.out.print(wynik.get(i) + " ");
        }
        System.out.println();
    }
}
