package pl.imiajd.jurczyk;

public class Nauczyciel extends Osoba{
    private int pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, int pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja=pensja;
    }

    public String toString(){
        return super.toString() +" pensja: " + this.pensja;
    }

    public int getPensja(){
        return this.pensja;
    }

}
