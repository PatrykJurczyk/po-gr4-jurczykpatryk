package pl.imiajd.jurczyk;

public class Adres {
    private String ulica;
    private  Integer numer_domu;
    private  Integer numer_mieszkania = 0;
    private String miasto;
    private String kod_pocztowy;

    public Adres(String ulica, Integer numer_domu, String miasto, String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    public Adres(String ulica, Integer numer_domu, Integer numer_mieszkania, String miasto, String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    public void pokaz(){
        System.out.println(this.kod_pocztowy+" "+this.miasto);
        if(this.numer_mieszkania!=0) {
            System.out.println("Ulica: "+this.ulica+" Numer mieszaknia: "+this.numer_mieszkania+" Numer domu: "+this.numer_domu);
        }
        else {
            System.out.println(this.ulica+" "+this.numer_domu);
        }
    }
    public boolean przed(Adres x){
        int[] z = new int[6];
        StringBuilder pom = new StringBuilder();
        StringBuilder pom1 = new StringBuilder();
        for(int i=0; i<z.length; i++){
            if(i==2){
                continue;
            }
            else {
                pom.append(x.kod_pocztowy.charAt(i));
                pom1.append(this.kod_pocztowy.charAt(i));
            }
        }
        int d = Integer.parseInt(pom.toString());
        int d1 = Integer.parseInt(pom1.toString());
        if(d1<d){
            return true;
        }
        else{
            return false;
        }
    }
}