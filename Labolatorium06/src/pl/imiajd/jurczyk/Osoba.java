package pl.imiajd.jurczyk;

public class Osoba {
    private String nazwisko;
    private int rokUrodzenia;

    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }
    public String toString(){
        return "Nazwisko "+this.nazwisko+" rok urodzenia: "+this.rokUrodzenia;
    }
    public String getNazwisko(){
        return this.nazwisko;
    }
    public int getRokUrodzenia(){
        return this.rokUrodzenia;
    }
}
