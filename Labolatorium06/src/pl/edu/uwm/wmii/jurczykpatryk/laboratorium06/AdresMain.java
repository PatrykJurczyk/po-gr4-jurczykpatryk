package pl.edu.uwm.wmii.jurczykpatryk.laboratorium06;

import pl.imiajd.jurczyk.Adres;

public class AdresMain {
    public static void main(String[] args){
        Adres x = new Adres("Piaskowa",32,"Olsztyn","76-712");
        Adres y = new Adres("Słoneczna",9,34,"Olsztyn","76-711");
        x.pokaz();
        y.pokaz();
        System.out.println(y.przed(x));
        System.out.println(x.przed(y));
    }


}
