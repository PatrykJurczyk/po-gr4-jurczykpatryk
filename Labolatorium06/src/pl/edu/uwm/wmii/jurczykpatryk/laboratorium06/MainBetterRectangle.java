package pl.edu.uwm.wmii.jurczykpatryk.laboratorium06;

import pl.imiajd.jurczyk.BetterRectangle;

public class MainBetterRectangle {
    public static void main(String[] args) {
        BetterRectangle x=new BetterRectangle(1, 2, 3, 4);
        System.out.println(x.getPerimeter());
        System.out.println(x.getArea());
    }
}
