package pl.edu.uwm.wmii.jurczykpatryk.laboratorium06;

import pl.imiajd.jurczyk.Osoba;
import pl.imiajd.jurczyk.Student;
import pl.imiajd.jurczyk.Nauczyciel;


public class MainOSN {
    public static void main(String[] args){
        Osoba x = new Osoba("Kowalski",2000);
        Student y = new Student("Brzeczyszczykiewicz", 1999, "Informatyka");
        Nauczyciel z = new Nauczyciel("Kimono", 1975, 4538);
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(x.getNazwisko());
        System.out.println(y.getKierunek());
        System.out.println(z.getPensja());
    }
}
