package pl.edu.uwm.wmii.jurczykpatryk.laboratorium09;

public class ArrayUtilPomocnicze<T> {

    public static <T> void swap(T[] arr, int index1, int index2) {
        try {
            if (index1 != index2) {
                T temp = arr[index1];
                arr[index1] = arr[index2];
                arr[index2] = temp;
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Invalid index for given array!\n" + e.getMessage());
        }
    }

}