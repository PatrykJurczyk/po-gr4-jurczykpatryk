package pl.edu.uwm.wmii.jurczykpatryk.laboratorium09;

public class PairUtil<T> {

    public PairUtil() {
        first = null;
        second = null;
    }

    public PairUtil (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }
    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }
    static public <T> void swap(Pair<T> p){
        T temp = p.getFirst();
        p.setFirst(p.getSecond());
        p.setSecond(temp);
    }


    private T first;
    private T second;
}