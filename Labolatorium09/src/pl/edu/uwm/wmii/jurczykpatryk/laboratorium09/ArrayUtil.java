package pl.edu.uwm.wmii.jurczykpatryk.laboratorium09;

import java.time.LocalDate;
import java.util.ArrayList;


public class ArrayUtil<T extends Comparable<T>> {
    public static <T extends Comparable<T>> boolean isSorted(T[] a){
        for(int i=0; i< a.length; i++){
            if(a[i].compareTo(a[i+1]) == -1){
                return true;
            }
            else{
                break;
            }
        }
        return false;
    }
    public static <T extends Comparable<T>> int binSearch(T[] x, T y){
        int z = 0;
        for(int i=0; i< x.length; i++){
            if(x[i].equals(y)){
                z = i;
                break;
            }
            else {
                z = (-1);
            }
        }
        return z;
    }

    public static <T extends Comparable<T>> void selectionSort(T[] x){
        if(x == null || x.length == 0){
            return;
        }
        int n = x.length;
        int minIndex;
        for(int i = 0; i < n - 1; i++){
            minIndex = i;
            for(int j = i + 1; j < n; j++){
                if(x[j].compareTo(x[minIndex]) < 0) {
                    minIndex = j;

                }
            }
            if(minIndex != i){
                ArrayUtilPomocnicze.swap(x, minIndex, i);
            }
        }
    }
    public static <T extends Comparable<? super T>>  void mergeSort(ArrayList<T> x){
        if (x.size() > 1) {
            ArrayList<T> y = new ArrayList<>();
            ArrayList<T> z = new ArrayList<>();
            boolean pom = true;
            while (!x.isEmpty()) {
                if (pom) {
                    y.add(x.remove(0));
                } else {
                    z.add(x.remove(x.size()/2));
                }
                pom = !pom;
            }
            mergeSort(y);
            mergeSort(z);
            while (!y.isEmpty() && !z.isEmpty()) {
                if(y.get(0).compareTo(z.get(0)) <= 0){
                    x.add(y.remove(0));
                }
                else {
                    x.add(z.remove(0));
                }
            }
            if(!y.isEmpty()){
                x.addAll(y);
            }
            else if (!z.isEmpty()){
                x.addAll(z);
            }
        }
    }
    
    public static void main(String[] args){
        Integer[] zbint = new Integer[6];
        LocalDate[] zbdat = new LocalDate[3];
        ArrayList x = new ArrayList();
        ArrayList y = new ArrayList();
        int liczba = 5;
        LocalDate data = LocalDate.of(2000,12,12);
        zbint[0] = 6;
        zbint[1] = 2;
        zbint[2] = 5;
        zbint[3] = 1;
        zbint[4] = 3;
        zbint[5] = 4;
        zbdat[0] = LocalDate.of(2001,12,12);
        zbdat[1] = LocalDate.of(2000,12,13);
        zbdat[2] = LocalDate.of(2000,12,14);
        x.add(7);
        x.add(5);
        x.add(9);
        x.add(0);
        x.add(2);
        x.add(1);
        x.add(4);
        x.add(6);
        y.add(LocalDate.of(2001,12,12));
        y.add(LocalDate.of(2000,12,13));
        y.add(LocalDate.of(2000,12,14));
        // Zadanie 3.
        System.out.println(isSorted(zbint));
        System.out.println(isSorted(zbdat));
        //Zadanie 5.
        selectionSort(zbint);
        selectionSort(zbdat);
        // Zadanie 3.
        System.out.println(isSorted(zbint));
        System.out.println(isSorted(zbdat));
        // Zadanie 4.
        System.out.println(binSearch(zbint,liczba));
        System.out.println(binSearch(zbdat,data));
        //Zadanie 6.
        mergeSort(x);
        mergeSort(y);
    }
}
