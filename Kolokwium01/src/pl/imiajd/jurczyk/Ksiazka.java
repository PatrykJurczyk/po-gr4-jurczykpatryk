package pl.imiajd.jurczyk;

public class Ksiazka implements Comparable<Object>, Cloneable{
    private String tytul;
    private Autor autor;
    private double cena;

    public Ksiazka(String tytul, Autor autor, double cena) {
        this.tytul = tytul;
        this.autor = autor;
        this.cena = cena;
    }

    @Override
    public String toString() {
        return "Ksiazka [" +
                "tytul='" + tytul + '\'' +
                ", autor=" + autor +
                ", cena=" + cena +
                ']';
    }

    @Override
    public int compareTo(Object o) {
        Ksiazka ksiazka = (Ksiazka) o;
        if (this.autor.compareTo(ksiazka.autor) == 0) {
            if(this.cena > ksiazka.cena){
                return 1;
            }
            if(this.cena < ksiazka.cena){
                return -1;
            }
            if(this.cena == ksiazka.cena){
                return 0;
            }
            return this.tytul.compareTo(ksiazka.tytul);
        }
        return this.autor.compareTo(ksiazka.autor);
    }
}
