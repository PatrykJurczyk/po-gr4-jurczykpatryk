package pl.imiajd.jurczyk;

import java.util.ArrayList;
import java.util.Collections;

public class TestKsiazka {
    public static void main(String[] args){
        ArrayList<Ksiazka> ksiazka = new ArrayList<>();

        ksiazka.add(new Ksiazka("Bracia",new Autor("Kinono", "kim@gmail.com", 'M') , 34.54));
        ksiazka.add(new Ksiazka("Kraina",new Autor("PaniX", "panix@gmail.com", 'K'), 54.23));
        ksiazka.add(new Ksiazka("Bracia",new Autor("PaniX", "panixds@gmail.com", 'K'), 54.22));
        ksiazka.add(new Ksiazka("Podrecznik",new Autor("Kowalski", "kowa@gmail.com", 'M') , 122.55));

        for(Ksiazka ks: ksiazka){
            System.out.println(ks);
        }
        System.out.println();
        Collections.sort(ksiazka);
        System.out.println();
        for(Ksiazka ks: ksiazka){
            System.out.println(ks);
        }

    }
}
