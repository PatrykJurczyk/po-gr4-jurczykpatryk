package pl.imiajd.jurczyk;

import java.util.ArrayList;
import java.util.Collections;

public class TestAutor {
    public static void main(String[] args){
        ArrayList<Autor> osoba = new ArrayList<>();

        osoba.add(new Autor("Kinono", "kim@gmail.com", 'M'));
        osoba.add(new Autor("PaniX", "panix@gmail.com", 'K'));
        osoba.add(new Autor("PaniX", "panixds@gmail.com", 'K'));
        osoba.add(new Autor("Kowalski", "kowa@gmail.com", 'M'));

        for(Autor au: osoba){
            System.out.println(au);
        }
        System.out.println();
        Collections.sort(osoba);
        System.out.println();
        for(Autor au: osoba){
            System.out.println(au);
        }
    }
}
