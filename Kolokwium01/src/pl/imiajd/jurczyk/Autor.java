package pl.imiajd.jurczyk;


public class Autor implements Comparable<Object>, Cloneable{
    private String nazwa;
    private String email;
    private char plec;

    public Autor(String nazwa, String email, char plec) {
        this.nazwa = nazwa;
        this.email = email;
        this.plec = plec;
    }

    public char getPlec() {
        return plec;
    }
    public String getEmail() {
        return email;
    }
    public String getNazwa() {
        return nazwa;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
    public void setPlec(char plec) {
        this.plec = plec;
    }

    @Override
    public String toString() {
        return "Autor [" +
                "nazwa='" + nazwa + '\'' +
                ", email='" + email + '\'' +
                ", plec=" + plec +
                ']';
    }

    @Override
    public int compareTo(Object o) {
        Autor autor = (Autor)o;
        if(this.nazwa.equals(autor.nazwa)){
            return  String.valueOf(this.plec).compareTo(String.valueOf(autor.plec));
        }
        return this.nazwa.compareTo(autor.nazwa);
    }
}
