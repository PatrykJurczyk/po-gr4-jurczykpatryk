package pl.imiajd.jurczyk;

import java.util.LinkedList;

public class Zadanie2 {

    public static void main(String[] args) {
        LinkedList<String> books = new LinkedList<>();
        books.add("Weronika postanawia umrzeć");
        books.add("Miasto Kości");
        books.add("Igrzyska Śmierci");
        books.add("Cudownie tu i teraz");
        books.add("Mechaniczny Anioł");
        books.add("Zaklinacz Czasu");
        System.out.println(books);
        redukuj(books,2);
        System.out.println(books);

    }
    public static void redukuj (LinkedList<String> book,int x) {
        LinkedList<String> pom = (LinkedList<String>) book.clone();
        int z = book.size();
        if (x < 2) {
            for (int i = 1; i <= z; i++) {
                if (x == 0) {
                    System.out.println("Zadna nie zostala usunieta :)");
                    break;
                } else if (x == 1) {
                    book.remove(0);
                }
            }
        }else {
            book.clear();
            for (int i = 0; i <z; i++) {
                if ((i+1) % x != 0) {
                    book.add(pom.get(i));
                }
            }
        }
    }
}