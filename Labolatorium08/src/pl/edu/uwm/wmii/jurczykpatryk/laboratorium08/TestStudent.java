package pl.edu.uwm.wmii.jurczykpatryk.laboratorium08;

import pl.imiajd.jurczyk.Osoba;
import pl.imiajd.jurczyk.Student;
import java.util.Arrays;
import java.time.LocalDate;


public class TestStudent {
    public static void main(String[] args){
        Student[] student = new Student[5];

        student[0] = new Student("Kimono", LocalDate.of(2010,9,23),4.23);
        student[1] = new Student("Kimono", LocalDate.of(2010,9,24), 4.22);
        student[2] = new Student("Brzeczyszczykiewicz", LocalDate.of(2004,9,15),5.45);
        student[3] = new Student("Kowalski", LocalDate.of(2004,9,15),5.46);
        student[4] = new Student("Jurczyk", LocalDate.of(2000,4,12),3.98);

        for(Student stu: student){
            System.out.println(stu);
        }
        System.out.println();

        Arrays.sort(student);

        for(Osoba os: student){
            System.out.println(os);
        }
    }
}
