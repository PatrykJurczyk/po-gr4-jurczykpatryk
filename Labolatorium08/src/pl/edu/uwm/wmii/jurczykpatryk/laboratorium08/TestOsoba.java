package pl.edu.uwm.wmii.jurczykpatryk.laboratorium08;

import java.time.LocalDate;
import java.util.Arrays;
import pl.imiajd.jurczyk.Osoba;

public class TestOsoba {
    public static void main(String[] args){
        Osoba[] grupa = new Osoba[5];

        grupa[0] = new Osoba("Kimono", LocalDate.of(2010,9,24));
        grupa[1] = new Osoba("Kimono", LocalDate.of(2010,9,23));
        grupa[2] = new Osoba("Brzeczyszczykiewicz", LocalDate.of(2004,9,15));
        grupa[3] = new Osoba("Kowalski", LocalDate.of(2004,9,15));
        grupa[4] = new Osoba("Jurczyk", LocalDate.of(2000,4,12));

        for(Osoba os: grupa){
            System.out.println(os);
        }
        System.out.println();
        Arrays.sort(grupa);
        for(Osoba os: grupa){
            System.out.println(os + "]");
        }
    }
}
