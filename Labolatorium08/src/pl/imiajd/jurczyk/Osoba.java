package pl.imiajd.jurczyk;

import java.time.LocalDate;
import java.util.Objects;

public class Osoba implements Comparable<Object>, Cloneable{
    private String nazwisko;
    private LocalDate dataUrodzena;

    public Osoba(String nazwisko, LocalDate dataUrodzena){
        this.nazwisko = nazwisko;
        this.dataUrodzena = dataUrodzena;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Osoba osoba = (Osoba) o;
        return Objects.equals(nazwisko, osoba.nazwisko) &&
                Objects.equals(dataUrodzena, osoba.dataUrodzena);
    }

    @Override
    public String toString() {
        return "Osoba[" +
                "nazwisko = " + nazwisko + ' ' +
                " dataUrodzena = " + dataUrodzena;
    }

    @Override
    public int compareTo(Object o) {
        Osoba osoba = (Osoba)o;
        if(this.nazwisko == ((Osoba) osoba).nazwisko){
            return this.dataUrodzena.compareTo(((Osoba) osoba).dataUrodzena);
        }
        return this.nazwisko.compareTo(((Osoba) osoba).nazwisko);
    }
}