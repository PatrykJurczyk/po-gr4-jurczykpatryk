package pl.imiajd.jurczyk;

import java.time.LocalDate;


public class Student extends Osoba implements Comparable<Object>, Cloneable{
    private double sredniaOcen;

    public Student(String nazwisko, LocalDate dataUrodzena, double sredniaOcen) {
        super(nazwisko, dataUrodzena);
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String toString() {
        return super.toString() +
                " sredniaOcen = " + sredniaOcen +
                ']';
    }

    @Override
    public int compareTo(Object o) {
        Student student = (Student)o;
         if(super.compareTo(o)==0){
             return String.valueOf(this.sredniaOcen).compareTo(String.valueOf(((Student)student).sredniaOcen));
         }
         return super.compareTo(o);
    }
}
